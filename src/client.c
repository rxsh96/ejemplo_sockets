#include "csapp.h"

int main(int argc, char **argv){

	int clientfd;
	char *port;
	char *host;
	char *filename;

	size_t lenFilename;
	size_t lenFile;

	rio_t rio;

	if(argc != 4){
		fprintf(stderr, "usage: %s <host> <port> <file name>\n", argv[0]);
		exit(0);
	}

	host = argv[1];
	port = argv[2];
	filename = argv[3];

	clientfd = Open_clientfd(host, port);
	Rio_readinitb(&rio, clientfd);

	//Bloque de envio de informacion
	printf("[Cliente] buscando %s en el servidor... \n", filename);
	lenFilename = strlen(filename);
	Rio_writen(clientfd, &lenFilename, 4);
	Rio_writen(clientfd, filename, lenFilename);

	//Bloque de entrega de informacion
	Rio_readnb(&rio, &lenFile, 4);
	printf("Enviando request... \n");

	if(lenFile <= 0){
		fprintf(stderr, "El servidor no contiene el archivo '%s'\n", filename);
	}
	else{
		int fTransferedDescriptor;
		char path[MAXLINE] = "./clientFiles/";
		//Contenido del archivo
		char *fileContent = (char *)Malloc(lenFile*sizeof(char));
		Rio_readnb(&rio, fileContent, lenFile);
		strcat(path, filename);
		fTransferedDescriptor = Open(path, O_WRONLY|O_CREAT, 0666);
		Rio_writen(fTransferedDescriptor, fileContent, lenFile);
		printf("Descarga Completa de '%s' en %s\n", filename, path);
		Free(fileContent);
		Close(fTransferedDescriptor);
	}

	Close(clientfd);
	exit(0);

}
