#include "csapp.h"

void fileTransfer(int connfd);

int main(int argc, char **argv){

	int listenfd, connfd;
	unsigned int clientlen;
	struct sockaddr_in clientaddr;
	struct hostent *hp;
	char *haddrp, *port;

	if (argc != 2){
		fprintf(stderr, "usage: %s <port>\n", argv[0]);
		exit(0);
	}
	port = argv[1];

	listenfd = Open_listenfd(port);

	while (1){
		clientlen = sizeof(clientaddr);
		connfd = Accept(listenfd, (SA *)&clientaddr, &clientlen);
		/* Determine the domain name and IP address of the client */
		hp = Gethostbyaddr((const char *)&clientaddr.sin_addr.s_addr, sizeof(clientaddr.sin_addr.s_addr), AF_INET);
		haddrp = inet_ntoa(clientaddr.sin_addr);
		printf("server connected to %s (%s)\n", hp->h_name, haddrp);
		fileTransfer(connfd);
	}

	Close(connfd);
	exit(0);
}

void fileTransfer(int connfd){

	size_t lenFilename;
	size_t lenFile = 0;
	rio_t rio;

	Rio_readinitb(&rio, connfd);
	
	Rio_readnb(&rio, &lenFilename, 4);
	char *fileR = (char *)Malloc(lenFilename*sizeof(char));
	Rio_readlineb(&rio, fileR, lenFilename+1);
	char path[MAXLINE] = "./serverFiles/";
	strcat(path, fileR);
	fprintf(stderr, "Request del archivo '%s'\n", fileR);

	struct stat fileStat;
	if(stat(path, &fileStat) < 0 ){
		fprintf(stderr, "El servidor no contiene al archivo '%s'\n", fileR);
		Rio_writen(connfd, &lenFile, 4);
		Free(fileR);
		return;
	}

	size_t lenRead;
	int fd;

	lenFile = fileStat.st_size;
	fd = Open(path, O_RDONLY, 0);
	printf("Transfiriendo archivo '%s'...\n", fileR);
	char *fileContent = (char *)Malloc(lenFile*sizeof(char));
	lenRead = Rio_readn(fd, fileContent, lenFile);
	Rio_writen(connfd, &lenRead, 4);
	Rio_writen(connfd, fileContent, lenRead);
	printf("Archivo transferido!\n");

	Free(fileR);
	Free(fileContent);

}
